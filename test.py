#from zeus import Project
from zeus import *

z = Project("my_project", "my_src", "my_include", "my_lib")

# New file
f = z.add_src_file("my_file")

# Include libraries
f.add_library("assert")
f.add_library("stdlib")
f.add_library("stdint")
f.add_library("stdio")
f.add_library("stdbool")

# Add function
func_args = [CVariable("arg0", int32_type),
             CVariable("arg1", double_type)]
var0 = CVariable("var0", int32_type)
var1 = CVariable("var1", double_type)
func_body = CBlock([])
func_body.add_statement(CDeclareStat(var0));
func_body.add_statement(CAssignStat(var0, CIntLit(-2)));
func_body.add_statement(CDeclareStat(var1, CDoubleLit(-2.0)));
then_body = CBlock([])
then_body.add_statement(CPrintfStat(CStringLit("%s\n"), CStringLit("Yes")))

var2 = CVariable("flag", bool_type)
func_body.add_statement(CDeclareStat(var2, CBinaryOp("==", CIntLit(5), CIntLit(5))))
func_body.add_statement(CAssignStat(var2, CBinaryOp("==", CIntLit(5), CIntLit(5))))

func_body.add_statement(CAssert(var2))

cond = CIfThenElseStat(var2, then_body)
func_body.add_statement(cond)


func_body.add_statement(CAssignStat(var0, CBinaryOp("*", var0, CIntLit(5))))

func_body.add_statement(CReturnStat(var0));
func = CFunction("foo", int32_type, func_args, func_body)
f.add_function(func)

# Build

z.make_src_files()
z.make_interface_file("my_interface.py", "my_lib")
z.make_cmake_lists()

import os
os.chdir("my_build")
os.system("cmake ..")
os.system("make")
os.chdir("..")
import run

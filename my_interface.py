import os
import ctypes
import numpy

libname = "libmy_project"
libpath = os.path.join(os.path.dirname(__file__), "my_lib")
lib = numpy.ctypeslib.load_library(libname, libpath)

# === File: my_file.c

lib.foo.argtypes = [
    ctypes.c_int32,
    ctypes.c_double,
]
lib.foo.restype = ctypes.c_int32

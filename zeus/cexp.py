from .cobj import CObject

class CExpr(CObject):

    def __init__(self, value):
        self.value = value

    def make_src(self):
        if isinstance(self.value, CVariable) or isinstance(self.value, CLiteral):
            src = " " + exp.make_src()
        elif self.value is None:
            src = ""
        return src

class CAssignExpr(CExpr):

    def __init__(self, var, expr):
        self.var = var
        self.expr = expr

    def make_src(self):
        expr = self.expr.make_src()
        src = ["{} = {}".format(self.var.identifier, expr[0])]
        src += expr[1:]
        return src

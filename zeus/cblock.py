from .config import c_indent
from .cobj import CObject

class CBlock(CObject):

    def __init__(self, statements):
        self.statements = statements

    def add_statement(self, statement):
        self.statements.append(statement)

    def make_src(self):
        src = ["{"]
        for stat in self.statements:
            src_stat = stat.make_src()
            src_stat = [c_indent + stat for stat in src_stat]
            src += src_stat
        src += ["}"]
        return src

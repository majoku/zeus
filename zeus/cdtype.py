class CDataType:

    def __init__(self, name, ctypes_name=None):
        self.name = name
        self.ctypes_name = ctypes_name

int32_type = CDataType("int32_t", "c_int32")
int64_type = CDataType("int64_t", "c_int64")
double_type = CDataType("double", "c_double")
size_type = CDataType("size_t", "c_size_t")
bool_type = CDataType("bool", "c_bool")

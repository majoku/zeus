from .clib import CLibrary
from .pproc import PPIncludeStatement

class CFile:

    def __init__(self, name, filename):
        self.name = name
        self.filename = filename
        self.libs = []
        self.funcs = []

    def add_library(self, libname, stdlib=None, file_extension=".h"):
        lib = CLibrary(libname, stdlib, file_extension)
        self.libs.append(lib)

    def add_function(self, function):
        func = function
        self.funcs.append(func)

    def make_src_include(self):
        src = []
        for lib in self.libs:
            ppinc = PPIncludeStatement(lib)
            src_line = ppinc.make_src()
            src.append(src_line)
        return src

class CHeaderFile(CFile):

    def __init__(self, name, filename=None):
        if filename is None:
            filename = name + ".h"
        super().__init__(name, filename)

class CSourceFile(CFile):

    def __init__(self, name, filename=None):
        if filename is None:
            filename = name + ".c"
        super().__init__(name, filename)

    def make_src_function(self):
        src = []
        for func in self.funcs:
            src += [""]
            src_line = func.make_src()
            src += src_line
        return src

    def make_src(self):
        src_inc = self.make_src_include()
        src_func = self.make_src_function()
        src = src_inc + [""] + src_func
        return src

    def make_header_file(self):
        pass

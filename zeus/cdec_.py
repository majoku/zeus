from cstat import CStatement

class CDeclaration(CStatement):

    def __init__(self, variable):
        self.variable = variable

    def make_src(self):
        var = self.variable
        src = "{} {}".format(var.datatype, var.identifier)
        src = super().__init__(src)
        return src

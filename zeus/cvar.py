from .cobj import CObject

class CVariable(CObject):

    def __init__(self, identifier, datatype):
        self.identifier = identifier
        self.datatype = datatype

    def make_src(self):
        src = [self.identifier]
        return src

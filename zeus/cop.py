from .cexp import CExpr

BINARY_OPERATORS = [
        "+", "-", "*", "/", "%",
        "==", "!=", ">", "<", ">=", "<=",
        "&", "|",
        "&&", "||",
        ]

class CBinaryOp(CExpr):

    def __init__(self, operator, expr1, expr2):
        if operator not in BINARY_OPERATORS:
            raise ValueError("Unknown binary operator {}".format(operator))
        self.op = operator
        self.expr1 = expr1
        self.expr2 = expr2

    def make_src(self):
        expr1 = self.expr1.make_src()
        expr2 = self.expr2.make_src()
        src = expr1
        src[0] = "(" + src[0]
        src[-1] += " {} {}".format(self.op, expr2[0])
        src += expr2[1:]
        src[-1] += ")"
        return src

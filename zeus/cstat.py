from .cobj import CObject
from .cexp import CAssignExpr

class CStat(CObject):

    def make_src(self, src):
        src[-1] += ";"
        return src

class CExprStat(CStat):

    def __init__(self, expr):
        self.expr = expr

    def make_src(self):
        src = self.expr.make_src()
        src = super().make_src(src)
        return src

class CAssert(CStat):

    def __init__(self, expr):
        self.expr = expr

    def make_src(self):
        expr = self.expr.make_src()
        src = ["assert({}".format(expr[0])]
        src += expr[1:]
        src[-1] += ")"
        src = super().make_src(src)
        return src

class CDeclareStat(CStat):

    def __init__(self, variable, init_value=None):
        self.variable = variable
        self.init_value = init_value

    def make_src(self):
        src = ["{} {}".format(self.variable.datatype.name, self.variable.identifier)]
        if self.init_value:
            init = self.init_value.make_src()
            src[-1] += " = {}".format(init[0])
            src += init[1:]
        src = super().make_src(src)
        return src

class CAssignStat(CStat):

    def __init__(self, var, expr):
        self.expr = CAssignExpr(var, expr)

    def make_src(self):
        src = self.expr.make_src()
        src = super().make_src(src)
        return src

class CReturnStat(CStat):

    def __init__(self, expression):
        self.exp = expression

    def make_src(self):
        exp = self.exp.make_src()
        src = ["return {}".format(exp[0])]
        src += exp[1:]
        src = super().make_src(src)
        return src

class CIfThenElseStat(CStat):

    def __init__(self, expression, then_block, elifs=None, else_block=None):
        self.exp = expression
        self.then_block = then_block
        self.elifs = elifs
        self.else_block = else_block

    def make_src(self):
        exp = self.exp.make_src()
        src = ["if ({}".format(exp[0])]
        src += exp[1:]
        src[-1] += ")"
        src += self.then_block.make_src()
        if self.else_block is not None:
            src += ["else"]
            src += self.else_block.make_src()
        return src

class CForLoop(CStat):

    def __init__(self, init_expr, cond_expr, counter_expr, body):
        self.init_expr = init_expr
        self.cond_expr = cond_expr
        self.counter_expr = counter_expr
        self.body = body

    def make_src(self):
        init_expr = self.init_expr.make_src()
        src = ["for({}".format(init_expr[0])]
        src += init_expr[1:]
        src[-1] += "; "
        cond_expr = self.cond_expr.make_src()
        src += cond_expr
        src[-1] += "; "
        counter_expr = self.counter_expr.make_src()
        src += counter_expr
        src[-1] += ")"
        src += self.body.make_src()
        return src

class CPrintfStat(CStat):

    def __init__(self, format_literal, expr):
        self.fmt = format_literal
        self.expr = expr

    def make_src(self):
        fmt = self.fmt.make_src()
        exp = self.expr.make_src()
        src = ["printf({}".format(fmt[0])]
        src += fmt[1:]
        src[-1] += ", {}".format(exp[0])
        src += exp[1:]
        src[-1] += ")"
        src = super().make_src(src)
        return src

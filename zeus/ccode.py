class CVariable:

    def __init__(self, identifier, datatype):
        self.identifier = identifier
        self.datatype = datatype

    def declare(self):
        line = "{} {}".format(self.datatype, self.identifier)
        return line

    def define(self, value):
        line = "{} {} = {}".format(self.datatype, self.identifier, value)
        return line

class CBlock:
    pass

class CIfThenElse:

    def __init__(self):
        pass

class CFunction:

    def __init__(self, identifier, datatype, arguments):
        self.identifier = identifier
        self.datatype = datatype
        self.arguments = arguments

    def make_signature(self):
        lines = ["{} {}(".format(self.datatype, self.identifier)]
        for idx, arg in enumerate(self.arguments):
            if idx < (len(self.arguments)-1):
                lines.append("{}  {},".format(arg.datatype, arg.identifier))
            else:
                lines.append("{}  {})".format(arg.datatype, arg.identifier))
        return lines

if __name__ == "__main__":
    pass

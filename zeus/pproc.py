"""Preprocessor (PP) module"""
from .encaps import EncapsulatedCode
#from zeus import EncapsulatedCode
#from . import EncapsulatedCode

class PPStatement(EncapsulatedCode):
    prefix = "#"
    postfix = ""

class PPIncludeStatement(PPStatement):

    def __init__(self, library):
        self.library = library

    def make_src(self):
        lib = self.library
        brakets = "<>" if lib.stdlib else "\"\""
        src = "include {}{}{}{}".format(brakets[0], lib.libname, lib.file_extension, brakets[1])
        src = super().make_src(src)
        return src

class PPPragmaStatement(PPStatement):

    def __init__(self, statement):
        self.statement = statement

    def make_src(self):
        src = "pragma {}".format(self.statement)
        return src

class PPPragmaOnce(PPPragmaStatement):

    def __init__(self):
        super().__init__("once")

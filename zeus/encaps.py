class EncapsulatedCode:

    def make_src(self, src):
        if isinstance(src, str):
            src = self.prefix + src + self.postfix
        else:
            if len(src) == 0:
                src = [""]
            src[0] = self.prefix + src[0]
            src[-1] = src[-1] + self.postfix
        return src

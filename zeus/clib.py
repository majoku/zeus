ANSI_STDLIBS = [
        "assert",
        "complex",
        "ctype",
        "errno",
        "fenv",
        "float",
        "inttypes",
        "iso646",
        "limits",
        "locale",
        "math",
        "setjmp",
        "signal",
        "stdarg",
        "stdbool",
        "stdint",
        "stddef",
        "stdio",
        "stdlib",
        "string",
        "tgmath",
        "time",
        "wchar",
        "wctype"]

class CLibrary:

    def __init__(self, libname, stdlib=None, file_extension=".h"):
        if stdlib is None:
            stdlib = libname in ANSI_STDLIBS
        if stdlib:
            assert(file_extension == ".h")
        self.libname = libname
        self.stdlib = stdlib
        self.file_extension = file_extension

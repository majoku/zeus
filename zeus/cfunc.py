from .config import c_indent, py_indent
from .cobj import CObject

class CFunction(CObject):

    def __init__(self, identifier, datatype, arguments, body):
        self.identifier = identifier
        self.datatype = datatype
        self.arguments = arguments
        self.body = body

    def make_src_header(self):
        src = ["{} {}(".format(self.datatype.name, self.identifier)]
        for idx, arg in enumerate(self.arguments):
            src_line = "{}{} {}".format(c_indent, arg.datatype.name, arg.identifier)
            if idx < (len(self.arguments)-1):
                src_line += ","
            src.append(src_line)
        src[-1] = src[-1] + ")"
        return src

    def make_src_body(self):
        src = self.body.make_src()
        return src

    def make_src(self):
        src_header = self.make_src_header()
        src_body = self.make_src_body()
        src = src_header + src_body
        return src

    def make_python_interface(self, lib_id, ctypes="ctypes"):
        full_id = "{}.{}".format(lib_id, self.identifier)
        src = ["{}.argtypes = [".format(full_id)]
        for arg in self.arguments:
            src += ["{}{}.{},".format(py_indent, ctypes, arg.datatype.ctypes_name)]
        src  += ["]"]
        src += ["{}.restype = {}.{}".format(full_id, ctypes, self.datatype.ctypes_name)]
        return src

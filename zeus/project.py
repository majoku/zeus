import os

from .cfile import CSourceFile

class Project:

    def __init__(self, name, source_dir, include_dir, lib_dir):
        self.name = name
        self.src_dir = source_dir
        self.inc_dir = include_dir
        self.lib_dir = lib_dir
        self.src_files = []
        self.inc_files = []

    def add_src_file(self, name):
        src_file = CSourceFile(name)
        self.src_files.append(src_file)
        return src_file

    def make_src_files(self):
        for src_file in self.src_files:
            src = src_file.make_src()
            with open(os.path.join(self.src_dir, src_file.filename), "w") as f:
                f.writelines([x + "\n" for x in src])

    def make_interface_file(self, filename, libpath):
        with open(filename, "w") as f:
            f.write("import os\n")
            f.write("import ctypes\n")
            f.write("import numpy\n")
            f.write("\n")
            libname = "lib{}".format(self.name)
            f.write("libname = \"{}\"\n".format(libname))
            libpath = "os.path.join(os.path.dirname(__file__), \"{}\")".format(libpath)
            f.write("libpath = {}\n".format(libpath))
            f.write("lib = numpy.ctypeslib.load_library(libname, libpath)\n")
            f.write("\n")
            for src_file in self.src_files:
                f.write("# === File: {}\n".format(src_file.filename))
                for func in src_file.funcs:
                    f.write("\n")
                    src = func.make_python_interface("lib")
                    f.writelines([x + "\n" for x in src])

    def make_cmake_lists(self, filename="CMakeLists.txt", version=(3, 0)):
        with open(filename, "w") as f:
            f.write("cmake_minimum_required(VERSION {}.{})\n".format(version[0], version[1]))
            f.write("\n")
            f.write("include_directories({})\n".format(self.inc_dir))
            f.write("\n")
            f.write("file(GLOB SOURCES \"{}/*.c\")\n".format(self.src_dir))
            f.write("\n")
            f.write("add_library({} SHARED ${{SOURCES}})\n".format(self.name))
            f.write("\n")
            f.write("target_compile_options({} PRIVATE -O3)\n".format(self.name))
            f.write("target_compile_options({} PRIVATE -std=c11)\n".format(self.name))
            f.write("\n")
            f.write("set_target_properties(\n")
            f.write("    {} PROPERTIES\n".format(self.name))
            f.write("    LIBRARY_OUTPUT_DIRECTORY ${{PROJECT_BINARY_DIR}}/../{})".format(self.lib_dir))

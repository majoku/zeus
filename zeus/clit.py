from .cobj import CObject

class CLit(CObject):

    def __init__(self, value):
        self.value = value

    def make_src(self):
        src = [self.fmtstr.format(self.value)]
        return src

class CIntLit(CLit):
    fmtstr = "{:d}"

class CLongLit(CLit):
    fmtstr = "{:d}L"

class CDoubleLit(CLit):
    fmtstr = "{:f}"

class CCharLit(CLit):
    fmtstr = "'{:s}'"

class CStringLit(CLit):

    def __init__(self, value):
        value = value.replace("\n", "\\n")
        self.value = value

    fmtstr = "\"{:s}\""

class CBoolLit(CLit):

    def make_src(self):
        src = ["true" if self.value else "false"]
        return src
